//const MongoClient = require("mongodb").MongoClient;
const {MongoClient, ObjectID}= require("mongodb");

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if(err) return console.log("Unable to connect to mongodb server.");
  console.log("Successfully connected to the mongodb server");

  // db.collection("Todos").insertOne({
  //   text: "something to do",
  //   completed: false
  //
  // }, (err, result) => {
  //   if (err) return console.log('Unable to insert todo', err);
  //   console.log(JSON.stringify(result.ops, undefined, 2));
  // })

  // db.collection("Users").insertOne({
  //   name: "Tyler",
  //   age: 24,
  //   location: "Saint Michael, MN"
  //
  // }, (err, result) => {
  //   if (err) return console.log('Unable to insert todo', err);
  //   console.log(JSON.stringify(result.ops, undefined, 2));
  //   console.log(result.ops[0]._id.getTimestamp());
  // })

  db.close();
})
