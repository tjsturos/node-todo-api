//const MongoClient = require("mongodb").MongoClient;
const {MongoClient, ObjectID}= require("mongodb");

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if(err) return console.log("Unable to connect to mongodb server.");
  console.log("Successfully connected to the mongodb server");

// db.collection("Todos").findOneAndUpdate({_id: new ObjectID("58eeef7605a9d4d8d463b20a")}, {
//   $set: {
//     completed: true
//   }
// }, {
//   returnOriginal: false
// }
//
// ).then((result) => {
//   return console.log(JSON.stringify(result, undefined, 2));
// })


db.collection("Users").findOneAndUpdate({_id: new ObjectID("58ed9c9141175c01f07866dd")}, {
  $set: {
    name:"Tyler"
  },
  $inc: {
    age:1
  }
}, {
  returnOriginal: false
}

).then((result) => {
  return console.log(JSON.stringify(result, undefined, 2));
})


//  db.close();
})
