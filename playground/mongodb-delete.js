//const MongoClient = require("mongodb").MongoClient;
const {MongoClient, ObjectID}= require("mongodb");

MongoClient.connect("mongodb://localhost:27017/TodoApp", (err, db) => {
  if(err) return console.log("Unable to connect to mongodb server.");
  console.log("Successfully connected to the mongodb server");

// delete many
// db.collection("Users").deleteMany({name:"Tyler"}).then((result) => {
//   return console.log(result);
// });

//delete one
// db.collection("Todos").deleteOne({text:"Eat lunch"}).then((result) => {
//   return console.log(result);
// });

//find one and delete
db.collection("Users").findOneAndDelete({_id : new ObjectID("58ed9c7bd1d5aa2408aad8f3") }).then((result) => {
  return console.log(result);
});



//  db.close();
})
