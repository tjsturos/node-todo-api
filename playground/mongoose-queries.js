const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {User} = require('./../server/models/user');

var id = '58ef04afada19ef803b1c428';

// if(!ObjectID.isValid(id)) {
//   console.log('ID not valid!');
// }
// Todo.find({
//   _id: id
// }).then((todos) => {
//   console.log("Todos", todos);
// });
//
// Todo.findOne({
//   _id: id
// }).then((todo) => {
//   console.log("Todo", todo);
// });

User.findById(id).then((user) => {
  if(!user) return console.log("Unable to find user!");
  console.log("User by Id", user);
}).catch((e) => console.log(e));
