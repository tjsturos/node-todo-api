var prettify = (json) => JSON.stringify(json, undefined, 2);

module.exports.neatJSON = prettify;
module.exports.log = (toPrint, obj) => {
  if(obj) return console.log(toPrint, prettify(obj));
  console.log(toPrint);
}
