const expect = require("expect");
const request = require("body-parser");

const {app} = require('./../server');
const {Todo} = require('./../todo');

beforeEach((done) => {
  Todo.remove({}).then(() => done());
})

describe("POST /todos", async () => {
  it("Should create a new Todo", (done) => {
    var text = 'Test todo text';

    request(app)
      .post('/todos')
      .send({text})
      .expect(200)
      .expect((res) => {
        exepect(res.body.text).toBe(text);
      })
      .end((err, res) => {
        if(err) return done(err);
        try {
          const todos = await Todo.find();
          expect(todos.length).toBe(1);
          expect(todos[0].text).toBe(text);
          done();
        } catch (e) {
          done(e)
        }
      
      })
  })
})
